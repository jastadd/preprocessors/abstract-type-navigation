package org.jastadd.preprocessor;

import org.jastadd.option.BooleanOption;
import org.jastadd.option.ValueOption;
import org.jastadd.relast.ast.Document;
import org.jastadd.relast.ast.Program;
import org.jastadd.relast.compiler.CompilerException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;

import static org.jastadd.relast.compiler.Mustache.javaMustache;

/**
 * @author Johannes Mey - Initial contribution
 */
public class Main extends org.jastadd.relast.compiler.RelAstProcessor {

  private static final String MUSTACHE_TEMPLATE = "Navigation";

  protected ValueOption optionErrorHandling;
  protected BooleanOption optionPrintYaml;

  public Main() {
    super("Abstract Type Navigation Generator", false);
  }

  public static void main(String[] args) {
    try {
      int returnValue = new Main().run(args);
      System.exit(returnValue);
    } catch (CompilerException e) {
      System.err.println(e.getMessage());
      System.exit(-1);
    }
  }

  @Override
  protected void initOptions() {
    super.initOptions();
    optionErrorHandling = addOption(new ValueOption("errorHandling", "null|optional|exception|<exceptionClass> to return null, Optional.empty() or an exception (extending RuntimeException) in case of an error.").acceptAnyValue());
    optionErrorHandling.defaultValue("null");
    optionPrintYaml = addOption(new BooleanOption("printYaml", "create YAML file used by Mustache in the ouput directory"));
    optionPrintYaml.defaultValue(false);
  }

  @Override
  protected int processGrammar(Program program, Path inputBasePath, Path outputBasePath) throws CompilerException {

    boolean useExceptions = false;
    String exceptionName = "java.lang.RuntimeException";
    boolean useOptionals = false;

    String value = optionErrorHandling.value();
    switch (value) {
      case "null":
        // this is the default case
        break;
      case "optional":
        useOptionals = true;
        break;
      case "exception":
        useExceptions = true;
        exceptionName = "RuntimeException";
        break;
      default:
        // in any other case, the String is interpreted as the name of an exception
        useExceptions = true;
        exceptionName = value;
        String errorMsg = "Invalid argument '" + value + "' for parameter 'errorHandling': ";
        try {
          Class<?> exceptionClass = Class.forName(exceptionName);
          if (!RuntimeException.class.isAssignableFrom(exceptionClass)) {
            System.err.println("class " + exceptionClass);
            System.err.println(errorMsg + "Not a subclass of RuntimeException.");
            return -1;
          }
          exceptionClass.getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException e) {
          System.err.println(errorMsg + "Class not found (name must be qualified).");
          return -1;
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
          System.err.println(errorMsg + "Class does not have default constructor.");
          return -1;
        }
    }

    Document navigation = program.toNavigation(useExceptions, useOptionals, exceptionName);

    Path yamlFile = outputBasePath.resolve(navigation.getFileName());
    Path jragFile = yamlFile.getParent().resolve(MUSTACHE_TEMPLATE + ".jrag");

    if (optionPrintYaml.value() != null && optionPrintYaml.value()) {
      writeToFile(yamlFile, navigation.prettyPrint(false));
    }

    try {
      javaMustache(MUSTACHE_TEMPLATE, navigation.prettyPrint(), jragFile.toString());
    } catch (IOException e) {
      throw new CompilerException("Unable to expand template", e);
    }

    return 0;
  }
}

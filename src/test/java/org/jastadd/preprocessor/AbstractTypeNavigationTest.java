package org.jastadd.preprocessor;

import org.jastadd.relast.tests.RelAstProcessorTestBase;
import org.junit.jupiter.api.BeforeAll;

public class AbstractTypeNavigationTest extends RelAstProcessorTestBase {
  @BeforeAll
  static void init() {
    mainClass = Main.class;
  }
}
